var db;
function openDB(){
    
    console.log("openDB ") ;
    var request = indexedDB.open("clientes1",1);
    //db = request.result;
    


    request.onerror = function(event) {
        console.log("Database error: " + event.target.errorCode);
    };
    
    request.onsuccess = function(event) {
     console.log("Database onsuccess: "+request.result );
      db = request.result;
    };
    
    // This event is only implemented in recent browsers
    request.onupgradeneeded = function(event) { 
      console.log("onupgradeneeded");   
        
      var db = event.target.result;
    
      // Create an objectStore to hold information about our customers. We're
      // going to use "ssn" as our key path because it's guaranteed to be
      // unique.
     // var objectStore = db.createObjectStore("customers", { keyPath: "ssn" });
      var objectStore = db.createObjectStore("customers", { autoIncrement : true });
    
    
      // Create an index to search customers by name. We may have duplicates
      // so we can't use a unique index.
        objectStore.createIndex("name", "name", { unique: false });
    
       objectStore.createIndex("email", "email", { unique: true });
      
       objectStore.createIndex("matricula", "matricula", { unique: true });
    
       objectStore.createIndex("telefono", "telefono", { unique: false });
    
      
    };
}


function getClienteDB(clave,valor){
    //Ahora recogeremos los datos de nuestro almacén &quot;Productos&quot;
    var transaction = db.transaction(["customers"], "readwrite");
    var objectStore = transaction.objectStore("customers");
    
    
    var index = objectStore.index(clave);
    var singleKeyRange = IDBKeyRange.only(valor);
    
    // To use one of the key ranges, pass it in as the first argument of openCursor()/openKeyCursor()
    index.openCursor(singleKeyRange).onsuccess = function(event) {
      var cursor = event.target.result;
      if (cursor) {
        // Do something with the matches.
        cursor.continue();
        console.log("cursor avanza -> name: "+cursor.value.name+" email: "+cursor.value.email+" matricula: "+cursor.value.matricula+" telefono: "+cursor.value.telefono);
      }
    };
} 


function getObjectStoreClienteDB(clave,valor){
    //Ahora recogeremos los datos de nuestro almacén &quot;Productos&quot;
    var transaction = db.transaction(["customers"], "readwrite");
    var objectStore = transaction.objectStore("customers");
    
    
    var arrayObject = new Array();
    var index = objectStore.index(clave);
    var singleKeyRange = IDBKeyRange.only(valor);
    
    // To use one of the key ranges, pass it in as the first argument of openCursor()/openKeyCursor()
    /*index.openCursor(singleKeyRange).onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
        cursor.continue();
        console.log("cursor avanza ->"+"{  name:"+ cursor.value.name+" , email:"+ cursor.value.email +", matricula:"+ cursor.value.matricula+", telefono:"+ cursor.value.telefono+"}");
        
        arrayObject.push('{"name":"'+ cursor.value.name+'","email":"'+ cursor.value.email +'","matricula":"'+ cursor.value.matricula+'","telefono":"'+ cursor.value.telefono+'"}');
        for(var i=0; i<arrayObject.length; i++){         
            console.log("arrayObject:: "+arrayObject[i]);
        } 
      }
    };
    
    return arrayObject;*/
    
    return  index.openCursor(singleKeyRange);
} 


function addClienteDB(name,email,matricula,telefono){
    if (window.console.log) console.log("addCliente ... name: "+name+" email: "+email+" matricula: "+matricula+"telefono"+telefono);
    var transaction = db.transaction(["customers"], "readwrite");
    var obj = {  name: name, email: email, matricula: matricula, telefono:telefono};
    // Do something when all the data is added to the database.
    transaction.oncomplete = function(event) {
      console.log("All done!");
    };
    
    transaction.onerror = function(event) {
      console.log("addCliente ..."+name+" "+email +" "+ event.target.errorCode);
    };
    
    var objectStore = transaction.objectStore("customers");
    objectStore.add(obj);
}

function deleteClienteDB(name,email,matricula,telefono){
    
    
}


function updateClienteDB(clave,valor,newvalor){
    console.log("updateClienteDB ... clave: "+clave+" valor: "+valor+" newvalor: "+newvalor);
    var objectStore = db.transaction(["customers"], "readwrite").objectStore("customers");
    
   /* var array = getObjectStoreClienteDB(clave,valor);
    
    
    var obj = JSON.parse(array[0]);
    
    console.log("updateClienteDB ... obj: "+obj.name);*/
   
    request = getObjectStoreClienteDB("name",valor);
    
    request.onsuccess = function(event) {
      // Get the old value that we want to update
      var data = request.result;
      
      // update the value(s) in the object that you want to change
      if(clave=="name")
        data.name = newvalor;
       else if(clave=="email")
        data.email = newvalor;
       else if(clave=="matricula")
        data.matricula = newvalor;
       else if(clave=="telefono")
        data.telefono = newvalor;
    
      // Put this updated object back into the database.
      var requestUpdate = objectStore.put(data);
       requestUpdate.onerror = function(event) {
          console.log("addCliente ..."+name+" "+email +" "+ event.target.errorCode);
       };
       requestUpdate.onsuccess = function(event) {
            console.log("All done!");
       };
    };
}