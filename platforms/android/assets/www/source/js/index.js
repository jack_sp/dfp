var app = {

    initialize: function() {
    //  alert("0");
        this.bindEvents();
    },
    fileUploadTarget : "prueba"
    ,
    divFileUploadTarget:"prueba"
    ,
    supuestoTarget : "prueba"
    ,
    casoReclamacion: "prueba"
    ,
//    urlBase:"http://localhost:7001"
  urlBase:"http://wscentral-dfpas.rhcloud.com"
    ,
    arrayReclamaciones:["66"]
    ,
    validaMail: function(){
          var userinput = $('#emailaviso').val();
          var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

          var result = pattern.test(userinput);
          if (!result)
            app.toastMensaje("El email no es correcto",'danger');

          return !result;
        }
    ,
    ocultaCamposFechas:function(){
      $("#horasalidaprevista-row").hide();
      $("#retrasosalida-row").hide();
      $("#datellegadaprevista-row").hide();
      $("#horallegadaprevista-row").hide();
      $("#retrasollegada-row").hide();
    }
    ,
    visibleCamposFechas:function(){
      $("#horasalidaprevista-row").show();
      $("#retrasosalida-row").show();
      $("#datellegadaprevista-row").show();
      $("#horallegadaprevista-row").show();
      $("#retrasollegada-row").show();
    }
    ,
    bindEvents: function() {

      // var mlnuevareclamacion = document.getElementById('mlnuevareclamacion');
      //
      // mlnuevareclamacion.addEventListener('click',function(){
      //   app.cambioPagina('paso1');
      // },false);

    //  $('#footerDiv').load('./source/shared/Footer.html').trigger("create");

    var content = $.mobile.getScreenHeight() - $(".ui-header").outerHeight() - $(".ui-footer").outerHeight() - $(".ui-content").outerHeight() + $(".ui-content").height();


    $(".ui-content").height(content);

        var supuesto1 = document.getElementById('btsupuesto1');

        supuesto1.addEventListener('click',function(){
          app.supuestoTarget= "supuesto1";
          app.casoReclamacion="retraso";
          app.cambioPagina('supuesto1');
          app.visibleCamposFechas();
          $('#divUploadedFile8-row').hide();
        },false);

        var botonConsultaEstado = document.getElementById('botonConsultaEstado');

        botonConsultaEstado.addEventListener('click',function(){
          app.consultaProgresoReclamacion();
        },false);

        var supuesto2 = document.getElementById('btsupuesto2');

        supuesto2.addEventListener('click',function(){
          app.supuestoTarget= "supuesto2";
          app.casoReclamacion="cancel";
          app.cambioPagina('paso1');
          app.ocultaCamposFechas();
          $('#divUploadedFile8-row').hide();
        },false);

        var supuesto3 = document.getElementById('btsupuesto3');

        supuesto3.addEventListener('click',function(){
          app.supuestoTarget= "supuesto3";
          app.casoReclamacion="over";
          app.cambioPagina('paso1');
          app.ocultaCamposFechas();
          $('#divUploadedFile8-row').hide();
        },false);

        var supuesto4 = document.getElementById('btsupuesto4');

        supuesto4.addEventListener('click',function(){
          app.supuestoTarget= "supuesto4";
          app.casoReclamacion="equipaje";
          app.cambioPagina('paso1');
          app.ocultaCamposFechas();
            $('#divUploadedFile8-row').show();
        },false);

        var sendImagen = document.getElementById('sendImagen');

        sendImagen.addEventListener('click',function(){
          app.sendPhoto();
        },false);

        $('#observaciones').keypress(function(e) {
            var tval = $('#observaciones').val(),
                tlength = tval.length,
                set = 190,
                remain = parseInt(set - tlength);
            $('p').text(remain);
            if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
                $('textarea').val((tval).substring(0, tlength - 1))
            }
        });

        //para ios
        // $('#input').keydown( function(e){
        //     if ($(this).val().length >= 2) {
        //         $(this).val($(this).val().substr(0, max_chars));
        //     }
        // });
        //
        // $('#input').keyup( function(e){
        //     if ($(this).val().length >= 2) {
        //         $(this).val($(this).val().substr(0, max_chars));
        //     }
        // });


        $('#radio-choice-v-2a').click(function() {
            $('#tasupuesto1Cond2').hide();
        });

        $('#radio-choice-v-2b').click(function() {
            supuestoTarget= "supuesto1";
            $('#tasupuesto1Cond2').show();
        });

        $('#radio-choice-v-2c').click(function() {
          supuestoTarget= "supuesto1";
          $('#tasupuesto1Cond2').hide();
        });

         var supuestoCond = document.getElementById('supuestoCond');

         supuestoCond.addEventListener('click',function(){
           if($('#radio-choice-v-2a').is(':checked')) {
             supuestoTarget= "supuesto1";
              sAviso= "Lo sentimos pero si tu vuelo no ha tenido un retraso superior a 3 horas o se han declarado condiciones metorologicas  adversas no es posible reclamar por ello";

            $("#avisoCondiciones").html(sAviso);
            $("#avisoCondiciones").addClass("alert alert-danger");
             app.cambioPagina('error');
           }else if($('#radio-choice-v-2b').is(':checked')) {
             app.cambioPagina('paso1');
           }else if($('#radio-choice-v-2c').is(':checked')) {
             app.cambioPagina('paso1');
           }
         },false);

        var takePhoto = document.getElementById('takePhoto');

         takePhoto.addEventListener('click',function(){
           app.fileUploadTarget = "uploadedFile";
           app.divFileUploadTarget = "divUploadedFile";
           app.takePhoto();
         },false);

        // var takePhoto2 = document.getElementById('takePhoto2');
        //
        // takePhoto2.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile2";
        //   app.divFileUploadTarget = "divUploadedFile2";
        //   app.takePhoto();
        // },false);

        // var takePhoto3 = document.getElementById('takePhoto3');
        // takePhoto3.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile3";
        //   app.divFileUploadTarget = "divUploadedFile3";
        //   app.takePhoto();
        // },false);

        // var takePhoto4 = document.getElementById('takePhoto4');
        // takePhoto4.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile4";
        //   app.divFileUploadTarget = "divUploadedFile4";
        //   app.takePhoto();
        // },false);

        // var takePhoto5 = document.getElementById('takePhoto5');
        // takePhoto5.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile5";
        //   app.divFileUploadTarget = "divUploadedFile5";
        //   app.takePhoto();
        // },false);

        // var takePhoto6 = document.getElementById('takePhoto6');
        // takePhoto6.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile6";
        //   app.divFileUploadTarget = "divUploadedFile6";
        //   app.takePhoto();
        // },false);


        // var takePhoto8 = document.getElementById('takePhoto8');
        // takePhoto8.addEventListener('click',function(){
        //   app.fileUploadTarget = "uploadedFile8";
        //   app.divFileUploadTarget = "divUploadedFile8";
        //   app.takePhoto();
        // },false);


        var botonPaso1 = document.getElementById('botonPaso1');
        botonPaso1.addEventListener('click',function(){
          if($("#nombre").val()=='' ||
            $("#apellidos").val()=='' ||
            $("#emailaviso").val()=='' ||
            $("#telefono").val()=='')
              app.toastMensaje("Todos los campos son obligatorios",'danger');
            else{
              var result  = app.validaMail();

              if (!result)
                app.cambioPagina('contacto');
            }

        },false);

        var botonContacto = document.getElementById('botonContacto');
        botonContacto.addEventListener('click',function(){
          if($("#idvuelo").val()!='' &&
            $("#aeropuertosalida").val()!='' &&
            $("#retrasosalida").val()!='' &&
            $("#datesalidaprevista").val()!='' &&
            $("#horasalidaprevista").val()!='' &&
            $("#datellegadaprevista").val()!='' &&
            $("#AeropuertoLlegada").val()!='' &&
            $("#retrasollegada").val()!='' &&
            $("#horallegadaprevista").val()!='' &&
            app.casoReclamacion == 'retraso' ){
              app.cambioPagina('contacto1');
            }else if($("#idvuelo").val()!='' &&
              $("#aeropuertosalida").val()!='' &&
              $("#datesalidaprevista").val()!='' &&
              $("#AeropuertoLlegada").val()!='' &&
              app.casoReclamacion != 'retraso' ){
                app.cambioPagina('contacto1');
            }else{
                app.toastMensaje("Todos los campos son obligatorios",'danger');
            }

        },false);


    },
    toastMensaje:function(message,tipo){
      var $toast = $('<div class="ui-loader  ui-body-e ui-corner-all"><h3>' + message + '</h3></div>');
      var colorBackground = '';
      if(tipo=='danger')
        colorBackground = '#cc0000';

      $toast.css({
         display: 'block',
         color:'#fff',
         background: colorBackground,
         opacity: 0.90,
         position: 'fixed',
         padding: '7px',
         'text-align': 'center',
         width: '270px',
         left: ($(window).width() - 284) / 2,
         top: $(window).height() / 2 - 20
      });

      var removeToast = function(){
         $(this).remove();
      };

      $toast.click(removeToast);

      $toast.appendTo($.mobile.pageContainer).delay(2000);
      $toast.fadeOut(400, removeToast);
    },
    cambioPagina: function(marcador){
      $.mobile.changePage("index.html#" + marcador, {
        transition : "none"
      })
    }
    ,getReclamacion: function(claimId){
      // $.ajax({
      //   type: "GET",
      //   url: app.urlBase+"/rest/claimservices/getClaimDetail",
      //   data: "claimId="+claimId,
      //   success: function(data) {
      //       $("#nombrePasajero").html(data.pasajero.nombre);
      //       $("#numeroReferenciaReclamacion").html(data.id);
      //       $("#estadoRelamacion").html(data.estadoDTO.nombreEstado);
      //       $("#descripcionEstado").html(La reclamación ha sido admitida, creemos muy posible sea concedida la indenización. Te mantendremos al tanto de todos los cambios sobre la misma.);
      //       $("#imagenBarra").attr("src","./source/img/paso"+data.estadoDTO.secEstado+".jpg");
      // 	   console.log(data);
      //   }
      // });

      $("#nombrePasajero").html($("#nombre").val());
      $("#numeroReferenciaReclamacion").html($("#numeroReferenciaReclamacion").val());
      $("#estadoRelamacion").html("Admitida");
      $("#descripcionEstado").html("La reclamación ha sido admitida, creemos muy posible sea concedida la indenización. Te mantendremos al tanto de todos los cambios sobre la misma.");
      $("#imagenBarra").attr("src","./source/img/paso2.jpg");

    },getReclamaciones: function(claimId){
      // $.ajax({
      //   type: "GET",
      //   url: app.urlBase+"/rest/claimservices/getClaimDetails",
      //   data: "arrayReclamaciones="+claimId,
      //   success: function(data) {
      //     var html = "";
      //
      //     $.each(data, function (index, value) {
      //       html = html + "<div data-role='collapsible' id='imgProgresoReclamacion"+value.id+"'><p>N&uacute;mero de referencia : <b id='numeroReferenciaReclamacion'>"+value.codigoReclamacion+value.id+"</b></p><div class='ui-grid-a' ><div class='ui-block-a'  style='padding: 40px;'><p>Actualmente tu reclamación esta en estado <b id='estadoRelamacion"+value.id+"'>"+value.estadoDTO.nombreEstado+"</b></p><br><p id ='descripcionEstado'>"+value.estadoDTO.descripcionEstado+"</p></div><div class='ui-block-b'><img  src='./source/img/paso"+value.estadoDTO.secEstado+".jpg' /></div></div></div>";
      //     });
      //
      //     $("#allClaims").html(html);
      //
      //     // $('#collapsibleSet').collapsibleset('refresh');
      //     $('#collapsibleSet').find('div[data-role=collapsible]').collapsible({refresh:true});
      //      console.log(html);
      //   }
      // });
      var html = "";

      html = html + "<div data-role='collapsible' ><p>N&uacute;mero de referencia : <b id='numeroReferenciaReclamacion'>"+$("#numeroReferenciaReclamacion").val()+"</b></p><div class='ui-grid-a' ><div class='ui-block-a'  style='padding: 40px;'><p>Actualmente tu reclamación esta en estado <b id='estadoRelamacion'>Admitida</b></p><br><p id ='descripcionEstado'>La reclamación ha sido admitida, creemos muy posible sea concedida la indenización. Te mantendremos al tanto de todos los cambios sobre la misma.</p></div><div class='ui-block-b'><img  src='./source/img/paso2.jpg' /></div></div></div>";

      $("#allClaims").html(html);
    },

     selectPicture: function () {
        navigator.camera.getPicture(
            function(uri) {
                var img = document.getElementById('uploadedFile');
                img.style.visibility = "visible";
                img.style.display = "block";
                img.src = uri;
                document.getElementById('camera_status').innerHTML = "Success";
            },
            function(e) {
                console.log("Error getting picture: " + e);
                document.getElementById('camera_status').innerHTML = "Error getting picture.";
            },
            {  quality: 50, destinationType: navigator.camera.DestinationType.FILE_URI, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});
    },
    consultaProgresoReclamacion: function() {
      $.mobile.loading("show", {
                    text : 'Consultando',
                    textVisible : true,
                    theme : 'z',
                    textonly : false,
                    html : ""
                });
  //   	alert("1");
        var img = document.getElementById('visto');
        var fileURL = img.src;
    //      alert("2"+fileURL);
      function win(r) {
  //        alert("win");
  //        alert("r"+r);
   //
  //        alert("Code = " + r.responseCode);
  //  alert("Response = " + r.response);
  //  alert("Sent = " + r.bytesSent);
          $.mobile.loading('hide');
        //  app.cambioPagina('contacto3');
        if(r.response!="null"){
          var value = JSON.parse(r.response);
            var html =  "<div  id='imgProgresoReclamacion'><div class='ui-grid-a' ><div class='ui-block-a'  style='padding: 40px;'><p>Actualmente tu reclamación esta en estado <b >"+value.nombreEstado+"</b></p><br><p id ='descripcionEstado'>"+value.descripcionEstado+"</p></div><div class='ui-block-b'><img  src='./source/img/paso"+value.secEstado+".jpg' /></div></div></div>";

          //alert(html);
          $("#allClaims").html(html);
        }else{
          app.toastMensaje("No existe código de reclamación o email , por favor compruebalo",'danger');
        }
    }
  //  alert("3");
    function fail(error) {
        $.mobile.loading('hide');

      html =  "<div  id='imgProgresoReclamacion"+value.id+"'><p>N&uacute;mero de referencia : <b id='numeroReferenciaReclamacion'>"+value.codigoReclamacion+value.id+"</b></p><div class='ui-grid-a' ><div class='ui-block-a'  style='padding: 40px;'><p>Actualmente tu reclamación esta en estado <b id='estadoRelamacion"+value.id+"'>"+value.estadoDTO.nombreEstado+"</b></p><br><p id ='descripcionEstado'>"+value.estadoDTO.descripcionEstado+"</p></div><div class='ui-block-b'><img  src='./source/img/paso"+value.estadoDTO.secEstado+".jpg' /></div></div></div>";

          $.mobile.loading('hide');
          $("#allClaims").html(html);
    }

//    var uri = encodeURI("http://10.0.2.2:8080/dfpas2/rest/adjuntoservices/getClaimDetails");
     var uri = encodeURI("http://wscentral-dfpas.rhcloud.com/rest/adjuntoservices/getClaimDetails");

    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=fileURL.substr(fileURL.lastIndexOf('/')+1);
    options.mimeType="image/png";
    options.chunkedMode = false;
    options.headers = {
       Connection: "close"
    };

    var params = {};
    params.referenciaProgresoReclamacion = $("#referenciaProgresoReclamacion").val();
    params.referenciaEmailProgresoReclamacion = $("#referenciaEmailProgresoReclamacion").val();
    params.localephone= navigator.language;

    options.params= params;


    var headers={'headerParam':'headerValue'};
    var ft = new FileTransfer();
    options.headers = headers;
  ///alert("enviando");
    ft.upload(fileURL, uri, win, fail, options);
    //alert("enviado");
    },
    sendPhoto: function() {
    	$.mobile.loading("show", {
                    text : 'Enviando documentos',
                    textVisible : true,
                    theme : 'z',
                    textonly : false,
                    html : ""
                });
  //  	alert("1");
        var img = document.getElementById('uploadedFile');
        var fileURL = img.src;
    //   alert("2"+fileURL);
      function win(r) {
        // alert("win");
        // alert("r"+r);
      		$.mobile.loading('hide');
          app.cambioPagina('contacto3');
          //alert("1");
      	$("#avisoResultadosContacto2").html("");
     			$("#avisoResultadosContacto2").removeClass("alert alert-danger");
     			$("#avisoResultadosContacto2").removeClass("alert alert-success");

          	sAviso = "<p class='denuncia'><b>Querid@ "+$('#nombre').val()+" <br> Se ha enviado una reclamación con exito a nuestra oficina. </b></p>";
          	sAviso1 = "En unos instantes recibir&aacute;s un correo confirm&aacute;ndote la informaci&oacute;n que acabas de enviar Y un c&oacute;digo de reclamaci&oacute;n con el que podrás consultar en el apartado mis reclamaciones en que estado est&aacute; la misma.A partir de ahora nuestro equipo de profesionales, trabajaran en tu caso.<br>";
          	sAviso1 = sAviso1+"Conseguimos indemnización en el 98% de los casos.<br>";
          	sAviso1 = sAviso1+"Gracias por tu confianza";



          if(r.response!="error"){

          	$("#avisoResultadosContacto2").html(sAviso);
          	$("#avisoResultadosContacto3").html(sAviso1);
          	$("#avisoResultadosContacto2").addClass("alert alert-success");
            $("#avisoResultadosContacto3").addClass("alert alert-success");
          }else{

            sAviso = "<p class='denuncia'>Por razones tecnicas transitorias, no nos es posible poner tu reclamación ponte en contacto con nosotros desde el apartado contacto</p>";
            $("#avisoResultadosContacto2").html(sAviso);
            $("#avisoResultadosContacto2").addClass("alert alert-danger");
            $("#avisoResultadosContacto3").addClass("alert alert-danger");
          }
        	$.mobile.loading('hide');
		}
		//alert("3");
    function fail(error) {
		    $.mobile.loading('hide');
		    app.cambioPagina('contacto3');
		   	$("#avisoResultadosContacto2").html("");
			$("#avisoResultadosContacto2").removeClass("alert alert-danger");

          sAviso = "<p class='denuncia'>Por razones tecnicas transitorias, no nos es posible poner tu reclamación ponte en contacto con nosotros desde el apartado contacto</p>";
          $("#avisoResultadosContacto2").html(sAviso);
          $("#avisoResultadosContacto2").addClass("alert alert-danger");
          $("#avisoResultadosContacto3").addClass("alert alert-danger");
          $.mobile.loading('hide');
		}

	//	var uri = encodeURI("http://10.0.2.2:8080/dfpas2/rest/adjuntoservices/enviaMail");
    var uri = encodeURI("http://wscentral-dfpas.rhcloud.com/rest/adjuntoservices/enviaMail");

		//alert("4");
		var options = new FileUploadOptions();

        options.fileKey="file";
        options.fileName=fileURL.substr(fileURL.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";
        //alert("4.0");
        options.chunkedMode = false;
        options.headers = {
           Connection: "close"
        };

    var params = {};
    params.nombre = $("#nombre").val();
    params.apellidos =$("#apellidos").val();
    params.email =  $("#emailaviso").val();
    params.telefono = $("#telefono").val();

    params.idvuelo = $("#idvuelo").val();
    params.aeropuertosalida =$("#aeropuertosalida").val();

    if($("#retrasosalida").val()!='')
    params.retrasosalida = $("#retrasosalida").val();

    if($("#horasalidaprevista").val()!='')
    params.horasalidaprevista = $("#horasalidaprevista").val();

    if($("#datesalidaprevista").val()!='')
    params.datesalidaprevista = $("#datesalidaprevista").val();

    if($("#datellegadaprevista").val()!='')
    params.datellegadaprevista = $("#datellegadaprevista").val();

    params.AeropuertoLlegada =$("#AeropuertoLlegada").val();

    if($("#retrasollegada").val()!='')
    params.retrasollegada = $("#retrasollegada").val();

    if($("#horallegadaprevista").val()!='')
    params.horallegadaprevista = $("#horallegadaprevista").val();

    params.casoReclamacion  = app.casoReclamacion;
    params.observaciones= $("#observaciones").val();
    params.localephone= navigator.language;

    options.params= params;


		var headers={'headerParam':'headerValue'};

		options.headers = headers;
			//alert("6");
		var ft = new FileTransfer();
		//alert("7");
		ft.onprogress = function(progressEvent) {
		    if (progressEvent.lengthComputable) {
		      loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
		    } else {
		      loadingStatus.increment();
		    }
		};
		//alert("Enviando " );
		ft.upload(fileURL, uri, win, fail, options);
		//alert("Enviando 1" );
    },


    takePhoto: function(){
    	//alert("take photo");
        navigator.camera.getPicture(app.onPhotoDataSuccess, app.onFail, { allowEdit:false, quality: 30,
             destinationType: navigator.camera.DestinationType.DATA_URL });
    },

    onPhotoDataSuccess: function(imageData) {
    		// $.mobile.loading("show", {
        //             text : 'Adjuntando foto',
        //             textVisible : true,
        //             theme : 'z',
        //             textonly : false,
        //             html : ""
        //         });
        //alert("success");


      var photo = document.getElementById(app.fileUploadTarget);
      photo.style.display = 'block';

      var divPhoto = document.getElementById(app.divFileUploadTarget);
      divPhoto.style.display = 'block';


      photo.src = "data:image/jpeg;base64," + imageData;

      // var sendPhoto = document.getElementById('sendPhoto');
      // sendPhoto.style.display = 'block';
      var siguiente = document.getElementById('siguiente');
      siguiente.style.display = 'block';
       $.mobile.loading('hide');
        // cambioPagina('contacto1');
    },

    onFail: function(message) {
      alert('Fallo al poner en funcionamiento la camara: ' + message);
    }

};
