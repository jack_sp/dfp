var app = {

    initialize: function() {
        this.bindEvents();
    },
    fileUploadTarget : "prueba"
    ,
    supuestoTarget : "prueba"
    ,

    bindEvents: function() {

      // var mlnuevareclamacion = document.getElementById('mlnuevareclamacion');
      //
      // mlnuevareclamacion.addEventListener('click',function(){
      //   app.cambioPagina('paso1');
      // },false);

        var supuesto1 = document.getElementById('btsupuesto1');

        supuesto1.addEventListener('click',function(){
          supuestoTarget= "supuesto1";
          app.cambioPagina('supuesto1');
        },false);

        var supuesto2 = document.getElementById('btsupuesto2');

        supuesto2.addEventListener('click',function(){
          supuestoTarget= "supuesto2";
          app.cambioPagina('paso1');
        },false);

        var supuesto3 = document.getElementById('btsupuesto3');

        supuesto3.addEventListener('click',function(){
          supuestoTarget= "supuesto3";
          app.cambioPagina('paso1');
        },false);

        var supuesto4 = document.getElementById('btsupuesto4');

        supuesto4.addEventListener('click',function(){
          supuestoTarget= "supuesto4";
          app.cambioPagina('paso1');
        },false);

        var sendImagen = document.getElementById('sendImagen');

        sendImagen.addEventListener('click',function(){
          app.sendPhoto();
        },false);



        $('#radio-choice-v-2a').click(function() {
            $('#tasupuesto1Cond2').hide();
        });

        $('#radio-choice-v-2b').click(function() {
            supuestoTarget= "supuesto1";
            $('#tasupuesto1Cond2').show();
        });

        $('#radio-choice-v-2c').click(function() {
          supuestoTarget= "supuesto1";
          $('#tasupuesto1Cond2').hide();
        });

         var supuestoCond = document.getElementById('supuestoCond');

         supuestoCond.addEventListener('click',function(){
           if($('#radio-choice-v-2a').is(':checked')) {
             supuestoTarget= "supuesto1";
              sAviso= "Lo sentimos pero si tu vuelo no ha tenido un retraso superior a 3 horas o se han declarado condiciones metorologicas  adversas no es posible reclamar por ello";

            $("#avisoCondiciones").html(sAviso);
            $("#avisoCondiciones").addClass("alert alert-danger");
             app.cambioPagina('error');
           }else if($('#radio-choice-v-2b').is(':checked')) {
             app.cambioPagina('paso1');
           }else if($('#radio-choice-v-2c').is(':checked')) {
             app.cambioPagina('paso1');
           }
         },false);

        var takePhoto = document.getElementById('takePhoto');

         takePhoto.addEventListener('click',function(){
           app.fileUploadTarget = "uploadedFile";
           app.takePhoto();
         },false);

        var takePhoto2 = document.getElementById('takePhoto2');

        takePhoto2.addEventListener('click',function(){
          app.fileUploadTarget = "uploadedFile2";
          app.takePhoto();
        },false);

        var takePhoto3 = document.getElementById('takePhoto3');
        takePhoto3.addEventListener('click',function(){
          app.fileUploadTarget = "uploadedFile3";
          app.takePhoto();
        },false);

        var takePhoto4 = document.getElementById('takePhoto4');
        // takePhoto4.addEventListener('click', app.takePhoto, false);
        takePhoto4.addEventListener('click',function(){
          app.fileUploadTarget = "uploadedFile4";
          app.takePhoto();
        },false);

        var takePhoto5 = document.getElementById('takePhoto5');
        takePhoto5.addEventListener('click',function(){
          app.fileUploadTarget = "uploadedFile5";
          app.takePhoto();
        },false);

        var takePhoto6 = document.getElementById('takePhoto6');
        takePhoto6.addEventListener('click',function(){
          app.fileUploadTarget = "uploadedFile6";
          app.takePhoto();
        },false);


        var botonPaso1 = document.getElementById('botonPaso1');
        botonPaso1.addEventListener('click',function(){
          if($("#nombre").val()=='' ||
            $("#apellidos").val()=='' ||
            $("#emailaviso").val()=='' ||
            $("#emailaviso").val()==''){
                 app.toastMensaje("Todos los campos son obligatorios",'danger');
            //  alert("error");
            }

            else
              app.cambioPagina('contacto');
        },false);


        $('#tweecool').tweecool({
          //settings
           username : 'realDonaldTrump',
           limit : 15,
           profile_image : true,
           show_time : true,
           show_media : true,
          show_media_size: 'thumb',  //values: small, large, thumb, medium
          show_actions: false,
          action_reply_icon: '&crarr;',
          action_retweet_icon: '&prop;',
          action_favorite_icon: '&#9733;',
          profile_img_url: 'profile', //Values: profile, tweet
          show_retweeted_text: false //This will show the original tweet in order to avoid any truncated text, and also the "RT @tweecool:" is removed which helps with 140 character limit
        });
    },

    cambioPagina: function(marcador){
      $.mobile.changePage("index.html#" + marcador, {
        transition : "none"
      })
    }
    ,
    toastMensaje: function(message,tipo){
      var $toast = $('<div class="ui-loader  ui-body-e ui-corner-all"><h3>' + message + '</h3></div>');
      var colorBackground = '';
      if(tipo=='danger')
        colorBackground = '#cc0000';

      $toast.css({
         display: 'block',
         color:'#fff',
         background: colorBackground,
         opacity: 0.90,
         position: 'fixed',
         padding: '7px',
         'text-align': 'center',
         width: '270px',
         left: ($(window).width() - 284) / 2,
         top: $(window).height() / 2 - 20
      });

      var removeToast = function(){
         $(this).remove();
      };

      $toast.click(removeToast);

      $toast.appendTo($.mobile.pageContainer).delay(2000);
      $toast.fadeOut(400, removeToast);
    },

     selectPicture: function () {
        navigator.camera.getPicture(
            function(uri) {
                var img = document.getElementById('uploadedFile');
                img.style.visibility = "visible";
                img.style.display = "block";
                img.src = uri;
                document.getElementById('camera_status').innerHTML = "Success";
            },
            function(e) {
                console.log("Error getting picture: " + e);
                document.getElementById('camera_status').innerHTML = "Error getting picture.";
            },
            {  quality: 50, destinationType: navigator.camera.DestinationType.FILE_URI, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});
    },

    sendPhoto: function() {
    	$.mobile.loading("show", {
                    text : 'Enviando correo',
                    textVisible : true,
                    theme : 'z',
                    textonly : false,
                    html : ""
                });
    	alert("1");
        var img = document.getElementById('uploadedFile');
        var fileURL = img.src;
        alert("2");
      function win(r) {
      		$.mobile.loading('hide');
      		cambioPagina('contacto3');
      		$("#avisoResultadosContacto2").html("");
 			$("#avisoResultadosContacto2").removeClass("alert alert-danger");
 			$("#avisoResultadosContacto2").removeClass("alert alert-success");

          	sAviso = "<p class='denuncia'><b>Se ha enviado un mail con exito a nuestra oficina. En caso de ser un caso urgente, es mejor llamar por teléfono</b></p>";
          	sAviso1 = "En unos instantes recibir&aacute;s un correo confirm&aacute;ndote la informaci&oacute;n que acabas de enviar .A partir de ahora nuestro equipo de profesionales, trabajaran para confeccionar un presupuesto sobre lo que te costara libarte de tu multa.<br>";
          	sAviso1 = sAviso1+"Este presupuesto es gratuito y si compromiso, en hemos llegado a hacer ahorrar hasta un 85% del importe de la multa.<br>";
          	sAviso1 = sAviso1+"Gracias por tu confianza";
          	$("#avisoResultadosContacto2").html(sAviso);
          	$("#avisoResultadosContacto3").html(sAviso1);
          	$("#avisoResultadosContacto2").addClass("alert alert-success");
        	$.mobile.loading('hide');
		}
		alert("3");
		function fail(error) {
		    $.mobile.loading('hide');
		    cambioPagina('contacto3');
		   	$("#avisoResultadosContacto2").html("");
			$("#avisoResultadosContacto2").removeClass("alert alert-danger");

          sAviso = "<p class='denuncia'>Por razones tecnicas transitorias, no nos es posible ofercer la informaci&oacute;n solicitada. Intentalo de nuevo m&aacute;s tarde</p>";
          $("#avisoResultadosContacto2").html(sAviso);
          $("#avisoResultadosContacto2").addClass("alert alert-danger");
          $.mobile.loading('hide');
		}

		var uri = encodeURI("http://10.0.2.2:8080/dfpas2/jsonServlet");
		//var uri = encodeURI("http://multasradar4.appspot.com/rest/mail");
		alert("4");
		var options = new FileUploadOptions();

        options.fileKey="file";
        options.fileName=fileURL.substr(fileURL.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";
        //alert("4.0");
        options.chunkedMode = false;
        options.headers = {
           Connection: "close"
        };

		// var email = $("#email").val()
		// var telf = $("#telf").val();
		// var calendario = $("#calendario").val();
    //
    //     var params = {};
    //     params.calendario = calendario;
    //     params.telf =telf;
    //     params.email = email;
    //     params.textarea = $("#textarea-1").val();
		//  options.params= params;

		// if(email=="" || !validarEmail( email )){
		// 			 	$("#avisoResultadosContacto").html("<p>Es obligatorio un correo electronico valido para poder avisarte</p>");
    //
		// 			 	$("#avisoResultadosContacto").addClass("alert alert-danger");
    //
		// 			 	 $.mobile.loading('hide');
		// 			 	return null;
		// 			}


		var headers={'headerParam':'headerValue'};

		options.headers = headers;
			alert("6");
		var ft = new FileTransfer();
		alert("7");
		ft.onprogress = function(progressEvent) {
		    if (progressEvent.lengthComputable) {
		      loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
		    } else {
		      loadingStatus.increment();
		    }
		};
		alert("Enviando " );
		ft.upload(fileURL, uri, win, fail, options);
		alert("Enviando 1" );
    },


    takePhoto: function(){
    	//alert("take photo");
        navigator.camera.getPicture(app.onPhotoDataSuccess, app.onFail, { allowEdit:false, quality: 50,
             destinationType: navigator.camera.DestinationType.DATA_URL });
    },

    onPhotoDataSuccess: function(imageData) {
    		// $.mobile.loading("show", {
        //             text : 'Adjuntando foto',
        //             textVisible : true,
        //             theme : 'z',
        //             textonly : false,
        //             html : ""
        //         });

      var  divUploadTarget  =  app.fileUploadTarget;

      var photo = document.getElementById(divUploadTarget);

      photo.style.display = 'block';

      photo.src = "data:image/jpeg;base64," + imageData;

      var sendPhoto = document.getElementById('sendPhoto');
      sendPhoto.style.display = 'block';
      var siguiente = document.getElementById('siguiente');
      siguiente.style.display = 'block';
       $.mobile.loading('hide');
        // cambioPagina('contacto1');
    },

    onFail: function(message) {
      alert('Fallo al poner en funcionamiento la camara: ' + message);
    }

};
